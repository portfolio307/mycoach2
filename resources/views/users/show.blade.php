@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-6">
                        <h1>{{__ ('mycoach.user.show_title')}}</h1>
                    </div>
                    <div class="col-6">
                        <a class="float-end" href="{{ route('users.edit', $user->id)}}">
                            <button type="button" class="btn btn-success">{{__ ('mycoach.user.edit_profile')}}</button>
                        </a>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">{{__ ('mycoach.user.show_title')}}</div>
                    <div class="card-body">

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{__ ('mycoach.user.name')}}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control"
                                       name="name" value="{{ $user->name }}" disabled>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{__ ('mycoach.user.email')}}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control"
                                       name="name" value="{{ $user->email }}" disabled>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{__ ('mycoach.user.account_type')}}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control"
                                       name="name"
                                       @if($user->account_type == 1)
                                           value="{{ __('mycoach.club.coach') }}"
                                       @else
                                           value="{{ __('mycoach.tournament.charge') }}"
                                       @endif
                                       disabled>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{__ ('mycoach.user.coach_id')}}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control"
                                       name="name" value="{{$coach_id}}" disabled>
                            </div>
                        </div>

{{--                        <div class="row mb-3">--}}
{{--                            <label for="name" class="col-md-4 col-form-label text-md-end">{{__ ('mycoach.user.club_id')}}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="name" type="text" class="form-control"--}}
{{--                                       name="name" value="@if($user->club_id!=null){{$user->club_id}} @else Brak klubu @endif" disabled>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div>{{__ ('mycoach.user.actual_photo')}}</div>
                        <div class="img-fluid d-flex justify-content-center">
                            @if ($user -> photo != null)
                                <img class="img-fluid p-3 " src="{{ asset('storage/' . $user -> photo) }}"
                                     alt="Zdjęcie profilu"/>
                            @else
                                <img class="img-fluid p-3" src="{{ asset('storage/photos/No_photo.jpg') }}"
                                     alt="Brak zdjęcie profilu"/>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
