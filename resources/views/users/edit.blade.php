@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{__ ('mycoach.user.edit_title')}}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('users.update', $user->id) }}"
                              enctype="multipart/form-data">
                            @csrf

                            <div class="row mb-3">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-end">{{__ ('mycoach.user.name')}}</label>
                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror"
                                           name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="email"
                                       class="col-md-4 col-form-label text-md-end">{{__ ('mycoach.user.email')}}</label>
                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           class="form-control @error('email') is-invalid @enderror"
                                           name="email" value="{{ $user->email }}">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="coach_id"
                                       class="col-md-4 col-form-label text-md-end">{{__ ('mycoach.user.coach_id')}}</label>
                                <div class="col-md-6">
                                    <select class="form-select" name="coach_id" id="coach_id">
                                        @foreach($coaches as $coach)
                                            <option value="{{$coach -> id}}">{{$coach -> name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="club_id"
                                       class="col-md-4 col-form-label text-md-end">{{ __('mycoach.auth.club_id') }}</label>
                                <div class="col-md-6">
                                    <select class="form-select" name="club_id" id="club_id">
                                        @if($clubs != [])
                                            @foreach($clubs as $club)
                                                <option value="{{$club -> id}}">{{$club -> name}}</option>
                                            @endforeach
                                        @else
                                            <option>{{__ ('mycoach.user.no_club')}}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="photo"
                                       class="col-md-4 col-form-label text-md-end">{{__ ('mycoach.user.photo')}}</label>
                                <div class="col-md-6">
                                    <input id="photo" type="file" accept="image/jpeg, .jpg, .png" class="form-control"
                                           name="photo">
                                </div>
                            </div>

                            <div>{{__ ('mycoach.user.actual_photo')}}</div>
                            <div class="img-fluid d-flex justify-content-center">
                                @if ($user -> photo != null)
                                    <img class="img-fluid p-3 " src="{{ asset('storage/' . $user -> photo) }}"
                                         alt="Zdjęcie profilu"/>
                                    @else
                                    <img class="img-fluid p-3" src="{{ asset('storage/photos/No_photo.jpg') }}"
                                         alt="Brak zdjęcie profilu"/>
                                @endif
                            </div>
                            <div class="row mb-0 d-flex justify-content-center">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('mycoach.button.save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
