<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="x-apple-disable-message-reformatting">
    <title></title>

    <style type="text/css">
        @media only screen and (min-width: 620px) {
            .u-row {
                width: 600px !important;
            }

            .u-row .u-col {
                vertical-align: top;
            }

            .u-row .u-col-33p33 {
                width: 199.98px !important;
            }

            .u-row .u-col-40 {
                width: 240px !important;
            }

            .u-row .u-col-50 {
                width: 300px !important;
            }

            .u-row .u-col-60 {
                width: 360px !important;
            }

            .u-row .u-col-100 {
                width: 600px !important;
            }

        }

        @media (max-width: 620px) {
            .u-row-container {
                max-width: 100% !important;
                padding-left: 0px !important;
                padding-right: 0px !important;
            }

            .u-row .u-col {
                min-width: 320px !important;
                max-width: 100% !important;
                display: block !important;
            }

            .u-row {
                width: 100% !important;
            }

            .u-col {
                width: 100% !important;
            }

            .u-col > div {
                margin: 0 auto;
            }
        }

        body {
            margin: 0;
            padding: 0;
        }

        table,
        tr,
        td {
            vertical-align: top;
            border-collapse: collapse;
        }

        p {
            margin: 0;
        }

        .ie-container table,
        .mso-container table {
            table-layout: fixed;
        }

        * {
            line-height: inherit;
        }

        a[x-apple-data-detectors='true'] {
            color: inherit !important;
            text-decoration: none !important;
        }

        table, td {
            color: #000000;
        }

        #u_body a {
            color: #0000ee;
            text-decoration: underline;
        }

        @media (max-width: 480px) {
            #u_content_image_1 .v-src-width {
                width: auto !important;
            }

            #u_content_image_1 .v-src-max-width {
                max-width: 90% !important;
            }

            #u_content_heading_1 .v-container-padding-padding {
                padding: 60px 10px 10px !important;
            }

            #u_content_heading_1 .v-text-align {
                text-align: center !important;
            }

            #u_content_heading_2 .v-container-padding-padding {
                padding: 10px 10px 0px !important;
            }

            #u_content_heading_2 .v-font-size {
                font-size: 24px !important;
            }

            #u_content_heading_2 .v-text-align {
                text-align: center !important;
            }

            #u_content_text_2 .v-text-align {
                text-align: center !important;
            }

            #u_content_heading_4 .v-container-padding-padding {
                padding: 60px 10px 0px !important;
            }

            #u_content_heading_4 .v-text-align {
                text-align: center !important;
            }

            #u_content_text_3 .v-container-padding-padding {
                padding: 5px 10px 10px !important;
            }

            #u_content_text_3 .v-text-align {
                text-align: center !important;
            }

            #u_content_button_2 .v-container-padding-padding {
                padding: 10px !important;
            }

            #u_content_button_2 .v-size-width {
                width: 65% !important;
            }

            #u_content_button_2 .v-text-align {
                text-align: center !important;
            }

            #u_content_image_2 .v-container-padding-padding {
                padding: 30px 5px 60px !important;
            }

            #u_content_image_2 .v-src-width {
                width: auto !important;
            }

            #u_content_image_2 .v-src-max-width {
                max-width: 90% !important;
            }

            #u_content_image_4 .v-src-width {
                width: auto !important;
            }

            #u_content_image_4 .v-src-max-width {
                max-width: 45% !important;
            }

            #u_content_text_9 .v-container-padding-padding {
                padding: 10px 10px 30px !important;
            }

            #u_content_image_5 .v-src-width {
                width: auto !important;
            }

            #u_content_image_5 .v-src-max-width {
                max-width: 35% !important;
            }

            #u_content_text_7 .v-container-padding-padding {
                padding: 10px 10px 30px !important;
            }

            #u_content_image_6 .v-src-width {
                width: auto !important;
            }

            #u_content_image_6 .v-src-max-width {
                max-width: 24% !important;
            }
        }
    </style>


</head>

<body class="clean-body u_body"
      style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #ffffff;color: #000000">

<table id="u_body"
       style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #ffffff;width:100%"
       cellpadding="0" cellspacing="0">
    <tbody>
    <tr style="vertical-align: top">
        <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">

            <div class="u-row-container" style="padding: 0px;background-color: #000001">
                <div class="u-row"
                     style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                    <div style="border-collapse: collapse;display: table;width: 100%;height: 100%;background-color: transparent;">
                        <div class="u-col u-col-100"
                             style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="height: 100%;width: 100% !important;">

                                <table id="u_content_image_1" style="font-family:'Rubik',sans-serif;"
                                       role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tbody>
                                    <tr>
                                        <td class="v-container-padding-padding"
                                            style="overflow-wrap:break-word;word-break:break-word;padding:80px 0px 0px;font-family:'Rubik',sans-serif;"
                                            align="left">

                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="v-text-align"
                                                        style="padding-right: 0px;padding-left: 0px;" align="center">

                                                        <img align="center" border="0" src="{{ $message->embed(public_path().'/storage/mail_images/image-1.png') }}"
                                                             alt="image" title="image"
                                                             style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 94%;max-width: 564px;"
                                                             width="564" class="v-src-width v-src-max-width"/>

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="u-row-container" style="padding: 0px;background-color: transparent">
                <div class="u-row"
                     style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                    <div style="border-collapse: collapse;display: table;width: 100%;height: 100%;background-color: transparent;">
                        <div class="u-col u-col-50"
                             style="max-width: 320px;min-width: 300px;display: table-cell;vertical-align: top;">
                            <div style="height: 100%;width: 100% !important;border-radius: 0px;-webkit-border-radius: 0px; -moz-border-radius: 0px;">

                                <table id="u_content_heading_1" style="font-family:'Rubik',sans-serif;"
                                       role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tbody>
                                    <tr>
                                        <td class="v-container-padding-padding"
                                            style="overflow-wrap:break-word;word-break:break-word;padding:63px 10px 10px;font-family:'Rubik',sans-serif;"
                                            align="left">

                                            <h1 class="v-text-align v-font-size"
                                                style="margin: 0px; line-height: 110%; text-align: left; word-wrap: break-word; font-family: inherit; font-size: 35px; ">
                                                <div><strong>Nowe zawody</strong></div>
                                                <div><strong>{{$tournament->name}}</strong></div>
                                            </h1>

                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="u-col u-col-50"
                             style="max-width: 320px;min-width: 300px;display: table-cell;vertical-align: top;">
                            <div style="height: 100%;width: 100% !important;border-radius: 0px;-webkit-border-radius: 0px; -moz-border-radius: 0px;">

                                <table id="u_content_heading_2" style="font-family:'Rubik',sans-serif;"
                                       role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tbody>
                                    <tr>
                                        <td class="v-container-padding-padding"
                                            style="overflow-wrap:break-word;word-break:break-word;padding:60px 10px 0px;font-family:'Rubik',sans-serif;"
                                            align="left">

                                            <h1 class="v-text-align v-font-size"
                                                style="margin: 0px; line-height: 130%; text-align: left; word-wrap: break-word; font-size: 22px; ">
                                                <strong>Dowiedz się więcej o wydarzeniu</strong></h1>

                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <table id="u_content_text_2" style="font-family:'Rubik',sans-serif;" role="presentation"
                                       cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tbody>
                                    <tr>
                                        <td class="v-container-padding-padding"
                                            style="overflow-wrap:break-word;word-break:break-word;padding:10px 10px 60px;font-family:'Rubik',sans-serif;"
                                            align="left">

                                            <div class="v-text-align v-font-size"
                                                 style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                <p style="font-size: 14px; line-height: 140%;"><strong><a rel="noopener"
                                                                                                          href="https://www.mycoach.com/tournament/{{$tournament->id}}"
                                                        target="_blank">Sprawdź &gt;</a></strong></p>
                                                </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="u-row-container" style="padding: 0px;background-color: #ecf0f1">
                <div class="u-row"
                     style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                    <div style="border-collapse: collapse;display: table;width: 100%;height: 100%;background-color: transparent;">
                        <div class="u-col u-col-60"
                             style="max-width: 320px;min-width: 360px;display: table-cell;vertical-align: top;">
                            <div style="background-color: #ecf0f1;height: 100%;width: 100% !important;border-radius: 0px;-webkit-border-radius: 0px; -moz-border-radius: 0px;">

                                <table id="u_content_heading_4" style="font-family:'Rubik',sans-serif;"
                                       role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tbody>
                                    <tr>
                                        <td class="v-container-padding-padding"
                                            style="overflow-wrap:break-word;word-break:break-word;padding:140px 10px 0px 20px;font-family:'Rubik',sans-serif;"
                                            align="left">

                                            <h1 class="v-text-align v-font-size"
                                                style="margin: 0px; line-height: 140%; text-align: left; word-wrap: break-word; font-size: 22px; ">
                                                <strong>Krótko o zawodach</strong></h1>

                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <table id="u_content_text_3" style="font-family:'Rubik',sans-serif;" role="presentation"
                                       cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tbody>
                                    <tr>
                                        <td class="v-container-padding-padding"
                                            style="overflow-wrap:break-word;word-break:break-word;padding:5px 20px 10px;font-family:'Rubik',sans-serif;"
                                            align="left">

                                            <div class="v-text-align v-font-size"
                                                 style="color: #3d3d3d; line-height: 140%; text-align: justify; word-wrap: break-word;">
                                                <p style="font-size: 14px; line-height: 140%;">{{$tournament->description}}</p>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <table id="u_content_button_2" style="font-family:'Rubik',sans-serif;"
                                       role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tbody>
                                    <tr>
                                        <td class="v-container-padding-padding"
                                            style="overflow-wrap:break-word;word-break:break-word;padding:10px 10px 10px 20px;font-family:'Rubik',sans-serif;"
                                            align="left">

                                            <div class="v-text-align" align="left">
                                                <a href="https://www.unlayer.com" target="_blank"
                                                   class="v-button v-size-width v-font-size"
                                                   style="box-sizing: border-box;display: inline-block;font-family:'Rubik',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; background-color: #000001; border-radius: 4px;-webkit-border-radius: 4px; -moz-border-radius: 4px; width:50%; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;font-size: 14px;">
                                                    <span style="display:block;padding:10px 20px;line-height:120%;"><strong><span
                                                                    style="font-size: 14px; line-height: 16.8px;">Sprawdź</span></strong></span>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="u-col u-col-40"
                             style="max-width: 320px;min-width: 240px;display: table-cell;vertical-align: top;">
                            <div style="background-color: #ecf0f1;height: 100%;width: 100% !important;border-radius: 0px;-webkit-border-radius: 0px; -moz-border-radius: 0px;">

                                <table id="u_content_image_2" style="font-family:'Rubik',sans-serif;"
                                       role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tbody>
                                    <tr>
                                        <td class="v-container-padding-padding"
                                            style="overflow-wrap:break-word;word-break:break-word;padding:60px 5px;font-family:'Rubik',sans-serif;"
                                            align="left">

                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="v-text-align"
                                                        style="padding-right: 0px;padding-left: 0px;" align="center">

                                                        <img align="center" border="0" src="{{ $message->embed(public_path().'/storage/mail_images/image-11.png') }}"
                                                             alt="image" title="image"
                                                             style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 100%;max-width: 230px;"
                                                             width="230" class="v-src-width v-src-max-width"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="u-row-container" style="padding: 0px;background-color: transparent">
                <div class="u-row"
                     style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                    <div style="border-collapse: collapse;display: table;width: 100%;height: 100%;background-color: transparent;">

                        <div class="u-col u-col-100"
                             style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="height: 100%;width: 100% !important;border-radius: 0px;-webkit-border-radius: 0px; -moz-border-radius: 0px;">

                                <table style="font-family:'Rubik',sans-serif;" role="presentation" cellpadding="0"
                                       cellspacing="0" width="100%" border="0">
                                    <tbody>
                                    <tr>
                                        <td class="v-container-padding-padding"
                                            style="overflow-wrap:break-word;word-break:break-word;padding:0px;font-family:'Rubik',sans-serif;"
                                            align="left">

                                            <table height="0px" align="center" border="0" cellpadding="0"
                                                   cellspacing="0" width="100%"
                                                   style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 5px solid #ffffff;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                <tbody>
                                                <tr style="vertical-align: top">
                                                    <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                        <span>&#160;</span>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="u-row-container" style="padding: 0px;background-color: transparent">
                <div class="u-row"
                     style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                    <div style="border-collapse: collapse;display: table;width: 100%;height: 100%;background-color: transparent;">
                        <div class="u-col u-col-33p33"
                             style="max-width: 320px;min-width: 200px;display: table-cell;vertical-align: top;">
                            <div style="height: 100%;width: 100% !important;border-radius: 0px;-webkit-border-radius: 0px; -moz-border-radius: 0px;">

                                <table id="u_content_image_4" style="font-family:'Rubik',sans-serif;"
                                       role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tbody>
                                    <tr>
                                        <td class="v-container-padding-padding"
                                            style="overflow-wrap:break-word;word-break:break-word;padding:10px;font-family:'Rubik',sans-serif;"
                                            align="left">

                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="v-text-align"
                                                        style="padding-right: 0px;padding-left: 0px;" align="center">

                                                        <img align="center" border="0" src="{{ $message->embed(public_path().'/storage/mail_images/image-8.png') }}"
                                                             alt="image" title="image"
                                                             style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 60%;max-width: 108px;"
                                                             width="108" class="v-src-width v-src-max-width"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <table id="u_content_text_9" style="font-family:'Rubik',sans-serif;" role="presentation"
                                       cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tbody>
                                    <tr>
                                        <td class="v-container-padding-padding"
                                            style="overflow-wrap:break-word;word-break:break-word;padding:10px;font-family:'Rubik',sans-serif;"
                                            align="left">

                                            <div class="v-text-align v-font-size"
                                                 style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                <p style="font-size: 14px; line-height: 140%;">Tretuj z
                                                    mycoach!</p>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="u-col u-col-33p33"
                             style="max-width: 320px;min-width: 200px;display: table-cell;vertical-align: top;">
                            <div style="height: 100%;width: 100% !important;border-radius: 0px;-webkit-border-radius: 0px; -moz-border-radius: 0px;">

                                <table id="u_content_image_5" style="font-family:'Rubik',sans-serif;"
                                       role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tbody>
                                    <tr>
                                        <td class="v-container-padding-padding"
                                            style="overflow-wrap:break-word;word-break:break-word;padding:10px 10px 8px;font-family:'Rubik',sans-serif;"
                                            align="left">

                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="v-text-align"
                                                        style="padding-right: 0px;padding-left: 0px;" align="center">

                                                        <img align="center" border="0" src="{{ $message->embed(public_path().'/storage/mail_images/image-9.png') }}"
                                                             alt="image" title="image"
                                                             style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 39%;max-width: 70.2px;"
                                                             width="70.2" class="v-src-width v-src-max-width"/>

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <table id="u_content_text_7" style="font-family:'Rubik',sans-serif;" role="presentation"
                                       cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tbody>
                                    <tr>
                                        <td class="v-container-padding-padding"
                                            style="overflow-wrap:break-word;word-break:break-word;padding:10px;font-family:'Rubik',sans-serif;"
                                            align="left">

                                            <div class="v-text-align v-font-size"
                                                 style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                <p style="font-size: 14px; line-height: 140%;">Dla nas najważniejszy
                                                    jesteś TY.</p>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="u-col u-col-33p33"
                             style="max-width: 320px;min-width: 200px;display: table-cell;vertical-align: top;">
                            <div style="height: 100%;width: 100% !important;border-radius: 0px;-webkit-border-radius: 0px; -moz-border-radius: 0px;">
                                <div style="box-sizing: border-box; height: 100%; padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;border-radius: 0px;-webkit-border-radius: 0px; -moz-border-radius: 0px;">
                                    <table id="u_content_image_6" style="font-family:'Rubik',sans-serif;"
                                           role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tbody>
                                        <tr>
                                            <td class="v-container-padding-padding"
                                                style="overflow-wrap:break-word;word-break:break-word;padding:10px 10px 9px;font-family:'Rubik',sans-serif;"
                                                align="left">

                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="v-text-align"
                                                            style="padding-right: 0px;padding-left: 0px;"
                                                            align="center">

                                                            <img align="center" border="0" src="{{ $message->embed(public_path().'/storage/mail_images/image-10.png') }}"
                                                                 alt="image" title="image"
                                                                 style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 29%;max-width: 52.2px;"
                                                                 width="52.2" class="v-src-width v-src-max-width"/>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <table style="font-family:'Rubik',sans-serif;" role="presentation" cellpadding="0"
                                           cellspacing="0" width="100%" border="0">
                                        <tbody>
                                        <tr>
                                            <td class="v-container-padding-padding"
                                                style="overflow-wrap:break-word;word-break:break-word;padding:10px 10px 60px;font-family:'Rubik',sans-serif;"
                                                align="left">

                                                <div class="v-text-align v-font-size"
                                                     style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                    <p style="font-size: 14px; line-height: 140%;">Do zobaczenia
                                                        na zawodahc!</p>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="u-row-container" style="padding: 0px;background-color: #ecf0f1">
                    <div class="u-row"
                         style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                        <div style="border-collapse: collapse;display: table;width: 100%;height: 100%;background-color: transparent;">

                            <div class="u-col u-col-100"
                                 style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                <div style="background-color: #ecf0f1;height: 100%;width: 100% !important;border-radius: 0px;-webkit-border-radius: 0px; -moz-border-radius: 0px;">

                                    <table style="font-family:'Rubik',sans-serif;" role="presentation" cellpadding="0"
                                           cellspacing="0" width="100%" border="0">
                                        <tbody>
                                        <tr>
                                            <td class="v-container-padding-padding"
                                                style="overflow-wrap:break-word;word-break:break-word;padding:60px 10px 10px;font-family:'Rubik',sans-serif;"
                                                align="left">

                                                <div align="center">
                                                    <div style="display: table; max-width:234px;">

                                                        <table align="left" border="0" cellspacing="0" cellpadding="0"
                                                               width="32" height="32"
                                                               style="width: 32px !important;height: 32px !important;display: inline-block;border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;margin-right: 15px">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td align="left" valign="middle"
                                                                    style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                                    <a href="https://facebook.com/" title="Facebook"
                                                                       target="_blank">
                                                                        <img src="{{ $message->embed(public_path().'/storage/mail_images/image-2.png') }}" alt="Facebook"
                                                                             title="Facebook" width="32"
                                                                             style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                        <table align="left" border="0" cellspacing="0" cellpadding="0"
                                                               width="32" height="32"
                                                               style="width: 32px !important;height: 32px !important;display: inline-block;border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;margin-right: 15px">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td align="left" valign="middle"
                                                                    style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                                    <a href="https://twitter.com/" title="Twitter"
                                                                       target="_blank">
                                                                        <img src="{{ $message->embed(public_path().'/storage/mail_images/image-3.png') }}" alt="Twitter"
                                                                             title="Twitter" width="32"
                                                                             style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                        <table align="left" border="0" cellspacing="0" cellpadding="0"
                                                               width="32" height="32"
                                                               style="width: 32px !important;height: 32px !important;display: inline-block;border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;margin-right: 15px">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td align="left" valign="middle"
                                                                    style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                                    <a href="https://linkedin.com/" title="LinkedIn"
                                                                       target="_blank">
                                                                        <img src="{{ $message->embed(public_path().'/storage/mail_images/image-4.png') }}" alt="LinkedIn"
                                                                             title="LinkedIn" width="32"
                                                                             style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                        <table align="left" border="0" cellspacing="0" cellpadding="0"
                                                               width="32" height="32"
                                                               style="width: 32px !important;height: 32px !important;display: inline-block;border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;margin-right: 15px">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td align="left" valign="middle"
                                                                    style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                                    <a href="https://instagram.com/" title="Instagram"
                                                                       target="_blank">
                                                                        <img src="{{ $message->embed(public_path().'/storage/mail_images/image-6.png') }}" alt="Instagram"
                                                                             title="Instagram" width="32"
                                                                             style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                        <table align="left" border="0" cellspacing="0" cellpadding="0"
                                                               width="32" height="32"
                                                               style="width: 32px !important;height: 32px !important;display: inline-block;border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;margin-right: 0px">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td align="left" valign="middle"
                                                                    style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                                    <a href="https://pinterest.com/" title="Pinterest"
                                                                       target="_blank">
                                                                        <img src="{{ $message->embed(public_path().'/storage/mail_images/image-7.png') }}" alt="Pinterest"
                                                                             title="Pinterest" width="32"
                                                                             style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <table style="font-family:'Rubik',sans-serif;" role="presentation" cellpadding="0"
                                           cellspacing="0" width="100%" border="0">
                                        <tbody>
                                        <tr>
                                            <td class="v-container-padding-padding"
                                                style="overflow-wrap:break-word;word-break:break-word;padding:20px 10px;font-family:'Rubik',sans-serif;"
                                                align="left">

                                                <table height="0px" align="center" border="0" cellpadding="0"
                                                       cellspacing="0" width="73%"
                                                       style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 1px solid #BBBBBB;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                    <tbody>
                                                    <tr style="vertical-align: top">
                                                        <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                            <span>&#160;</span>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <table style="font-family:'Rubik',sans-serif;" role="presentation" cellpadding="0"
                                           cellspacing="0" width="100%" border="0">
                                        <tbody>
                                        <tr>
                                            <td class="v-container-padding-padding"
                                                style="overflow-wrap:break-word;word-break:break-word;padding:10px 10px 30px;font-family:'Rubik',sans-serif;"
                                                align="left">

                                                <div class="v-text-align v-font-size"
                                                     style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                    <p style="font-size: 14px; line-height: 140%;">
                                                        email@website.com </p>
                                                    <p style="font-size: 14px; line-height: 140%;">+12 458 4658</p>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="u-row-container" style="padding: 0px;background-color: #000000">
                <div class="u-row"
                     style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                    <div style="border-collapse: collapse;display: table;width: 100%;height: 100%;background-color: transparent;">
                        <div class="u-col u-col-100"
                             style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="background-color: #000000;height: 100%;width: 100% !important;border-radius: 0px;-webkit-border-radius: 0px; -moz-border-radius: 0px;">

                                <table style="font-family:'Rubik',sans-serif;" role="presentation" cellpadding="0"
                                       cellspacing="0" width="100%" border="0">
                                    <tbody>
                                    <tr>
                                        <td class="v-container-padding-padding"
                                            style="overflow-wrap:break-word;word-break:break-word;padding:20px 10px;font-family:'Rubik',sans-serif;"
                                            align="left">

                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="v-text-align"
                                                        style="padding-right: 0px;padding-left: 0px;" align="center">

                                                        <img align="center" border="0" src="{{ $message->embed(public_path().'/storage/mail_images/image-5.png') }}"
                                                             alt="image" title="image"
                                                             style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 100%;max-width: 149px;"
                                                             width="149" class="v-src-width v-src-max-width"/>

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </td>
    </tr>
    </tbody>
</table>
</body>

</html>