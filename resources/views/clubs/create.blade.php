@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('mycoach.club.create_title') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('clubs.store') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('mycoach.club.name') }}</label>

                            <div class="col-md-6">
                                <input id="name" maxlength="300" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

{{--                        <div class="row mb-3">--}}
{{--                            <label for="coach_id" class="col-md-4 col-form-label text-md-end">{{ __('mycoach.club.coach') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="coach_id" maxlength="300" type="text" class="form-control @error('coach_id') is-invalid @enderror" name="coach_id" value="{{ old('coach_id') }}" required autocomplete="coach_id" autofocus>--}}

{{--                                @error('coach_id')--}}
{{--                                <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

                        <div class="row mb-3">
                            <label for="description" class="col-md-4 col-form-label text-md-end">{{ __('mycoach.club.description') }}</label>

                            <div class="col-md-6">
                                <textarea id="description" maxlength="2000" class="form-control @error('description') is-invalid @enderror" name="description"  required autofocus>{{ old('description') }}</textarea>

                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('mycoach.button.create') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
