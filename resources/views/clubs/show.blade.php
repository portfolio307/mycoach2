@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('mycoach.club.preview_title') }}</div>

                    <div class="card-body">

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('mycoach.club.name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control"
                                       name="name" value="{{ $club->name }}" disabled>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="coach_id" class="col-md-4 col-form-label text-md-end">{{ __('mycoach.club.coach') }}</label>

                            <div class="col-md-6">
                                <input id="coach_id" maxlength="300" type="text" class="form-control" name="coach_id" value="{{ $club->coach_id }}" disabled>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="description" class="col-md-4 col-form-label text-md-end">{{ __('mycoach.club.description') }}</label>

                            <div class="col-md-6">
                                <textarea id="description"
                                          class="form-control"
                                          name="description" disabled>
                                    {{ $club->description }}</textarea>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
