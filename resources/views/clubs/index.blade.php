@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-6">
                        <h1><i class="fa-solid fa-list"></i> {{ __('mycoach.club.index_title')}}</h1>
                    </div>
                    <div class="col-6">
                        <a class="float-end" href="{{ route('clubs.create') }}">
                            <button type="button" class="btn btn-success">{{ __('mycoach.button.add')}}</button>
                        </a>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">{{ __('mycoach.club.index_title')}}</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">id</th>
                                <th scope="col">{{ __('mycoach.club.name')}}</th>
                                <th scope="col">{{ __('mycoach.club.coach')}}</th>
                                <th scope="col">{{ __('mycoach.club.description')}}</th>
                                <th scope="col">{{__ ('mycoach.title.actions')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($clubs as $club)
                                <tr>
                                    <td>{{$club -> id}}</td>
                                    <td>{{$club -> name}}</td>
                                    <td>@if(!is_null($club -> coach_id)) {{$club -> coach_id }}@endif</td>
                                    <td>{{$club -> description}}</td>
                                    <td>
                                        <a href="{{ route('clubs.show', $club->id) }}">
                                            <button type="button" class="btn btn-primary"><i class="fa-solid fa-magnifying-glass"></i></button>
                                        </a>
                                        <a href="{{ route('clubs.edit', $club->id) }}">
                                            <button type="button" class="btn btn-warning"><i class="fa-solid fa-pen-to-square"></i></button>
                                        </a>


                                        <a href="tasks/{{$club->id}}" onclick="
                                            var result = confirm('Are you sure you want to delete this record?');

                                            if(result){
                                                event.preventDefault();
                                                document.getElementById('delete-form').submit();
                                            }">
                                            <button type="button" class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>
                                        </a>

                                        <form method="GET" id="delete-form"
                                              action="{{route('clubs.destroy', [$club->id])}}">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE">
                                        </form>

                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        {{$clubs->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
