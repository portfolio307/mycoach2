@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('mycoach.club.preview_title') }}</div>

                    <div class="card-body">
                        @if($club != [])
                            <div class="row mb-3">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-end">{{ __('mycoach.club.name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control"
                                           name="name" value="{{ $club->name }}" disabled>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="coach_id"
                                       class="col-md-4 col-form-label text-md-end">{{ __('mycoach.club.coach') }}</label>

                                <div class="col-md-6">
                                    <input id="coach_id" maxlength="300" type="text" class="form-control"
                                           name="coach_id" value="{{ $club->coach_id }}" disabled>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="description"
                                       class="col-md-4 col-form-label text-md-end">{{ __('mycoach.club.description') }}</label>

                                <div class="col-md-6">
                                <textarea id="description"
                                          class="form-control"
                                          name="description" disabled>
                                    {{ $club->description }}</textarea>
                                </div>
                            </div>
{{--                            @if ($user -> club_id == $club -> id)--}}
{{--                                <div class="row mb-0">--}}
{{--                                    <div class="col-md-6 offset-md-4">--}}
{{--                                        <a href="{{ route('clubs.leave_the_club', Auth::id()) }}">--}}
{{--                                            <button type="button" class="btn btn-danger">--}}
{{--                                                {{ __('mycoach.club.leave_the_club') }}--}}
{{--                                            </button>--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            @endif--}}
                            <div class="row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <a href="{{ route('clubs.leave_the_club', Auth::id()) }}">
                                    <button type="button" class="btn btn-danger">
                                        {{ __('mycoach.club.leave_the_club') }}
                                    </button>
                                    </a>
                                </div>
                            </div>

                        @elseif ($user -> account_type == 1)
                            <p>{{ __('mycoach.club.no_club') }}</p>
                            <h4>{{ __('mycoach.club.create_a_club') }}</h4>
                            <div class="row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <a href="{{ route('clubs.create')}}">
                                        <button type="button" class="btn btn-danger">
                                            {{ __('mycoach.button.create') }}
                                        </button>
                                    </a>
                                </div>
                            </div>

                        @elseif ($user -> account_type == 0)
                            <p>{{ __('mycoach.club.no_club') }}</p>
                            <h4>{{ __('mycoach.club.join_to_club') }}</h4>

                            <form method="POST" action="{{ route('clubs.join_club', Auth::id()) }}">
                                @csrf
                                <div class="row mb-3">
                                    <label for="club_id"
                                           class="col-md-4 col-form-label text-md-end">{{ __('mycoach.club.choose_a_club') }}</label>
                                    <div class="col-md-6">
                                        <select class="form-select" name="club_id" id="club_id">
                                            @foreach($all_clubs as $club)
                                                <option value="{{$club -> id}}">{{$club -> name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('mycoach.button.save') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
