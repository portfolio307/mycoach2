@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Register') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf

                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>
                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror" name="name"
                                           value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="email"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>
                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           class="form-control @error('email') is-invalid @enderror" name="email"
                                           value="{{ old('email') }}" required autocomplete="email">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="account_type"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Account type') }}</label>
                                <div class="col-md-6">
                                    <select class="form-select" name="account_type" id="account_type" required>
                                        <option value="0" selected>Podopieczny</option>
                                        <option value="1">Trener</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="coach_id"
                                       class="col-md-4 col-form-label text-md-end">{{ __('mycoach.auth.coach') }}</label>
                                <div class="col-md-6">
                                    <select class="form-select" name="coach_id" id="coach_id">
                                        <option value="" selected>Brak trenera</option>
                                        @foreach($coaches as $coach)
                                            <option value="{{$coach -> id}}">{{$coach -> name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            {{--                            <div class="row mb-3">--}}
                            {{--                                <label for="photo"--}}
                            {{--                                       class="col-md-4 col-form-label text-md-end">{{ __('mycoach.auth.photo') }}</label>--}}
                            {{--                                <div class="col-md-6">--}}
                            {{--                                    <input id="photo" type="file"--}}
                            {{--                                           class="form-control @error('photo') is-invalid @enderror" name="photo"--}}
                            {{--                                           accept="png, .jpg, .jpeg"--}}
                            {{--                                           value="{{ old('photo') }}" required autocomplete="photo">--}}
                            {{--                                    @error('photo')--}}
                            {{--                                    <span class="invalid-feedback" role="alert">--}}
                            {{--                                        <strong>{{ $message }}</strong>--}}
                            {{--                                    </span>--}}
                            {{--                                    @enderror--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}

                            <div class="row mb-3">
                                <label for="password"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>
                                <div class="col-md-6">
                                    <input id="password" type="password"
                                           class="form-control @error('password') is-invalid @enderror" name="password"
                                           required autocomplete="new-password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="password-confirm"
                                       class="col-md-4 col-form-label text-md-end">{{ __('Confirm Password') }}</label>
                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>

                            <div class="row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
