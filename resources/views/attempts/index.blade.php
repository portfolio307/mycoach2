@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-12">
                        <h1><i class="fa-solid fa-list"></i> {{ __('mycoach.attempt.list_of_attempts') }}</h1>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">{{ __('mycoach.attempt.list_of_attempts') }}</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">{{ __('mycoach.tournament.charge') }}</th>
                                <th scope="col">{{ __('mycoach.attempt.result') }}</th>
                                <th scope="col">{{ __('mycoach.attempt.rating') }}</th>
                                <th scope="col">{{ __('mycoach.title.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($attempts as $attempt)
                                <tr>
                                    <td>{{$attempt -> user_id -> name}}</td>
                                    <td>{{$attempt -> result}}</td>
                                    <td>{{$attempt -> rating}}</td>
                                    <td>
                                        <a href="{{ route('attempts.show', $attempt->id) }}">
                                            <button type="button" class="btn btn-primary"><i class="fa-solid fa-magnifying-glass"></i></button>
                                        </a>
                                        <a href="{{ route('attempts.edit', $attempt->id) }}">
                                            <button type="button" class="btn btn-warning"><i class="fa-solid fa-pen-to-square"></i></button>
                                        </a>

                                        <a href="tasks/{{$attempt->id}}" onclick="
                                            var result = confirm('Are you sure you want to delete this record?');

                                            if(result){
                                                event.preventDefault();
                                                document.getElementById('delete-form').submit();
                                            }">
                                            <button type="button" class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>
                                        </a>

                                        <form method="GET" id="delete-form"
                                              action="{{route('tournaments.destroy', [$attempt->id])}}">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE">
                                        </form>

                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        {{$attempts->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

