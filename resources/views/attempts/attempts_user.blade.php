@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('mycoach.attempt.result_for') }} dla {{$user -> name}}</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">id</th>
                                <th scope="col">{{ __('mycoach.tournament.tournament') }}</th>
                                <th scope="col">{{ __('mycoach.attempt.result') }}</th>
                                <th scope="col">{{ __('mycoach.attempt.rating') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($attempts as $attempt)
                                <tr>
                                    <td>{{$attempt -> id}}</td>
                                    <td>{{$attempt -> tournament -> name}}</td>
                                    <td>{{$attempt -> result}}</td>
                                    <td>{{$attempt -> rating}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
