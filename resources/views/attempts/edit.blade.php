@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('mycoach.attempt.create_title') }} dla {{$tournament->name}}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('attempts.store') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="user_id"
                                   class="col-md-4 col-form-label text-md-end">{{ __('mycoach.tournament.charge') }}</label>
                            <div class="col-md-6">
                                <input class="form-select" name="user_id" id="user_id" type="text" value="{{ $attempt -> user -> name }}" disabled>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="result" class="col-md-4 col-form-label text-md-end">{{ __('mycoach.attempt.result') }}</label>

                            <div class="col-md-6">
                                <input id="result" min="0" step="0.1" max="100000" type="number" class="form-control @error('result') is-invalid @enderror" name="result" value="{{ $attempt -> result }}" required autocomplete="result" autofocus>

                                @error('result')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="rating" class="col-md-4 col-form-label text-md-end">{{ __('mycoach.attempt.rating') }}</label>

                            <div class="col-md-6">
                                <input id="rating" min="0" max="10" step="1" type="number" class="form-control @error('rating') is-invalid @enderror" name="rating" value="{{ $attempt -> rating }}" required autocomplete="rating" autofocus>

                                @error('rating')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <input type="hidden" value="{{$tournament -> id}}" name="tournament_id">
                        <input type="hidden" value="kg" name="unit">

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('mycoach.button.create') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
