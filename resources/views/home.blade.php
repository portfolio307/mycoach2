@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="row">
                <div class="col-6">
                    <h1>{{__ ('mycoach.home.welcome', ['name' => $user -> name])}}</h1>
                </div>
                <div class="col-6">
                    @if (Auth::user()->account_type == 1)
                        <a class="float-end" href="{{ route('tournaments.create') }}">
                            <button type="button" class="btn btn-success">Dodaj</button>
                        </a>
                    @endif
                </div>
            </div>

            <div class="col-md-8">
                <div class="card mt-5">
                    <div class="card-header">{{__ ('mycoach.home.list_of_tournaments')}}</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">id</th>
                                <th scope="col">{{__ ('mycoach.tournament.name')}}</th>
                                <th scope="col">{{__ ('mycoach.tournament.description')}}</th>
                                <th scope="col">{{__ ('mycoach.tournament.date_and_time')}}</th>
                                <th scope="col">{{__ ('mycoach.title.actions')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tournaments as $tournament)
                                <tr>
                                    <td>{{$tournament -> id}}</td>
                                    <td>{{$tournament -> name}}</td>
                                    <td>{{$tournament -> description}}</td>
                                    <td>{{$tournament -> date_time}}</td>
                                    <td>
                                        <a href="{{ route('show', $tournament->id) }}">
                                            <button type="button" class="btn btn-primary">S</button>
                                        </a>

                                        <a href="{{ route('tournaments.edit', $tournament->id) }}">
                                            <button type="button" class="btn btn-warning">E</button>
                                        </a>

                                        <a href="tasks/{{$tournament->id}}" onclick="
                                            var result = confirm('Are you sure you want to delete this record?');

                                            if(result){
                                                event.preventDefault();
                                                document.getElementById('delete-form').submit();
                                            }">
                                            <button type="button" class="btn btn-danger">X</button>
                                        </a>

                                        <form method="GET" id="delete-form"
                                              action="{{route('tournaments.destroy', [$tournament->id])}}">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE">
                                        </form>

                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        {{--                        {{$tournaments->links()}}--}}
                    </div>
                </div>
            </div>

            @if ($charges != null)
                <div class="col-md-8 mt-5">
                    <div class="card">
                        <div class="card-header">{{__ ('mycoach.home.list_of_charges')}}</div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">id</th>
                                    <th scope="col">{{__ ('mycoach.user.name')}}</th>
                                    <th scope="col">{{__ ('mycoach.title.actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($charges as $charge)
                                    <tr>
                                        <td>{{$charge -> id}}</a></td>
                                        <td>{{$charge -> name}}</td>
                                        <td>
                                            <a href="{{ route('attempts.show_attempts_user', $charge -> id) }}"
                                               class="link-primary">
                                                {{__ ('mycoach.attempt.list_of_attempts')}}
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
