@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('mycoach.tournament.statistics') }} {{$tournament['name']}}</div>
                    <div class="card-body">
                        <p class="flex-row">{{ __('mycoach.tournament.avg_result')}}: <strong>{{$avg_result}} {{$tournament -> unit}}</strong></p>
                        <p>{{ __('mycoach.tournament.avg_rating')}}: <strong>{{$avg_rating}} {{$tournament -> unit}}</strong></p>
                        <p>{{ __('mycoach.tournament.number_attempts')}}: <strong>{{$number_result}}</strong></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
