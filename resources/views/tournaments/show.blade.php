@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('mycoach.title.tournament_preview') }}</div>

                    <div class="card-body">

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('mycoach.tournament.name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control"
                                       name="name" value="{{ $tournament->name }}" disabled>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="description" class="col-md-4 col-form-label text-md-end">{{ __('mycoach.tournament.description') }}</label>

                            <div class="col-md-6">
                                <textarea id="description"
                                          class="form-control"
                                          name="description" disabled>
                                    {{ $tournament->description }}</textarea>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="date_time" class="col-md-4 col-form-label text-md-end">{{ __('mycoach.tournament.date_and_time') }}</label>

                            <div class="col-md-6">
                                <input id="date_time" type="datetime-local"
                                       class="form-control" name="date_time"
                                       value="{{ $tournament->date_time }}" disabled>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="unit" class="col-md-4 col-form-label text-md-end">{{ __('mycoach.tournament.unit') }}</label>

                            <div class="col-md-6">
                                <textarea id="unit"
                                          class="form-control"
                                          name="unit" disabled>
                                    {{ $tournament->unit }}</textarea>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row col-12">
            <a class="float-end" href="{{ route('attempts.create', [$tournament->id]) }}">
                <button type="button" class="btn btn-success">{{__ ('mycoach.button.add_attempt')}}</button>
            </a>
            <a class="float-end" href="{{ route('tournaments.show_statistics', [$tournament->id]) }}">
                <button type="button" class="btn btn-success">{{__ ('mycoach.button.show_statistics')}}</button>
            </a>
        </div>

        <div class="row justify-content-center mt-5">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('mycoach.tournament.attempts') }}</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">{{ __('mycoach.tournament.charge') }}</th>
                                <th scope="col">{{ __('mycoach.attempt.result') }}</th>
                                <th scope="col">{{ __('mycoach.attempt.unit') }}</th>
                                <th scope="col">{{ __('mycoach.attempt.rating') }}</th>
                                <th scope="col">{{ __('mycoach.title.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($attempts as $attempt)
                                <tr>
                                    <td>{{$attempt -> user -> name}} </td>
                                    <td>{{$attempt -> result}}</td>
                                    <td>{{$attempt -> unit}}</td>
                                    <td>{{$attempt -> rating}}</td>
                                    <td>
                                        <a href="{{ route('attempts.edit', $attempt->id) }}">
                                            <button type="button" class="btn btn-warning">E</button>
                                        </a>
                                    </td>
                                </tr>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
