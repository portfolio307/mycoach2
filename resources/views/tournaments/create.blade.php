@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('mycoach.tournament.title') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('tournaments.store') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('mycoach.tournament.name') }}</label>
                            <div class="col-md-6">
                                <input id="name" maxlength="300" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="description" class="col-md-4 col-form-label text-md-end">{{ __('mycoach.tournament.description') }}</label>
                            <div class="col-md-6">
                                <textarea id="description" maxlength="2000" class="form-control @error('description') is-invalid @enderror" name="description"  required autofocus>{{ old('description') }}</textarea>
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="date_time" class="col-md-4 col-form-label text-md-end">{{ __('mycoach.tournament.date_and_time') }}</label>
                            <div class="col-md-6">
                                <input id="date_time" min="2022-01-01 00:00" step="00:30:00" type="datetime-local" class="form-control @error('date_time') is-invalid @enderror" name="date_time" value="{{ old('date_time') }}" required autocomplete="date_time">
                                @error('date_time')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="unit" class="col-md-4 col-form-label text-md-end">{{ __('mycoach.tournament.unit') }}</label>
                            <div class="col-md-6">
                                <input id="unit" maxlength="15" type="text" class="form-control @error('unit') is-invalid @enderror" name="unit" value="{{ old('unit') }}" required autocomplete="unit" autofocus>
                                @error('unit')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('mycoach.button.create') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
