@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-6">
                        <h1><i class="fa-solid fa-list"></i> {{ __('mycoach.title.list_of_tournaments')}}</h1>
                    </div>
                    @if (Auth::user()->account_type == 1)
                        <div class="col-6">
                            <a class="float-end" href="{{ route('tournaments.create') }}">
                                <button type="button" class="btn btn-success">Dodaj</button>
                            </a>
                        </div>
                    @endif
                </div>
                <div class="card">
                    <div class="card-header">{{ __('mycoach.home.list_of_tournaments') }}</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">id</th>
                                <th scope="col">{{ __('mycoach.tournament.name') }}</th>
                                <th scope="col">{{ __('mycoach.tournament.description') }}</th>
                                <th scope="col">{{ __('mycoach.tournament.date_and_time') }}</th>
                                <th scope="col">{{ __('mycoach.tournament.unit') }}</th>
                                <th scope="col">{{ __('mycoach.title.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tournaments as $tournament)
                                <tr>
                                    <td>{{$tournament -> id}}</td>
                                    <td>{{$tournament -> name}}</td>
                                    <td>{{$tournament -> description}}</td>
                                    <td>{{$tournament -> date_time}}</td>
                                    <td>@if($tournament -> unit != null)
                                            {{$tournament -> unit}}
                                        @else
                                            {{ __('mycoach.tournament.no_unit') }}
                                        @endif
                                    </td>
                                    <td>
                                        @if(Auth::id() == $tournament -> user_id)
                                            <a href="{{ route('show', $tournament->id) }}">
                                                <button type="button" class="btn btn-primary"><i class="fa-solid fa-magnifying-glass"></i></button>
                                            </a>
                                            <a href="{{ route('tournaments.edit', $tournament->id) }}">
                                                <button type="button" class="btn btn-warning"><i class="fa-solid fa-pen-to-square"></i></button>
                                            </a>

                                            <a href="tasks/{{$tournament->id}}" onclick="
                                            var result = confirm('Are you sure you want to delete this record?');

                                            if(result){
                                                event.preventDefault();
                                                document.getElementById('delete-form').submit();
                                            }">
                                                <button type="button" class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>
                                            </a>

                                            <form method="GET" id="delete-form"
                                                  action="{{route('tournaments.destroy', [$tournament->id])}}">
                                                {{csrf_field()}}
                                                <input type="hidden" name="_method" value="DELETE">
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        {{$tournaments->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection