<?php

namespace App\Http\Controllers;

use App\Models\Tournament;
use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        $user = Auth::user();
        if ($user['account_type'] == 1)
        {
            $charges = User::all()->where('coach_id', '=', Auth::id());
            $tournaments = Tournament::all()->where('user_id', '=',  $user['id']);
        }
        else if($user['account_type'] == 0)
        {
            $tournaments = Tournament::all()->where('user_id', '=',  $user['coach_id']);
            $charges = null;
        }
        else
        {
            $tournaments = [];
            $charges = null;
        }


        return (view("home", [
            'user' => $user,
            'tournaments' => $tournaments,
            'charges' => $charges
        ]));
    }
}
