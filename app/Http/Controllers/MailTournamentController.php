<?php

namespace App\Http\Controllers;

use App\Mail\TournamentEmail;
use http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailTournamentController extends Controller
{
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function index($email_to, $tournament)
    {
        $data["title"] = "title";
        $data["tournament"] = $tournament;

        $files = [
            public_path('storage/mail_images/image-1.png'),
            public_path('storage/mail_images/image-2.png'),
            public_path('storage/mail_images/image-3.png'),
            public_path('storage/mail_images/image-4.png'),
            public_path('storage/mail_images/image-5.png'),
            public_path('storage/mail_images/image-6.png'),
            public_path('storage/mail_images/image-7.png'),
            public_path('storage/mail_images/image-8.png'),
            public_path('storage/mail_images/image-9.png'),
            public_path('storage/mail_images/image-10.png'),
            public_path('storage/mail_images/image-11.png'),
        ];

        $collection_of_emails = $email_to->pluck('email');

        foreach ($collection_of_emails as $email_address) {
            Mail::send('emails.tournament_mail', $data, function ($message) use ($data, $files, $email_address, $tournament) {
                $message->to($email_address)
                    ->subject($data["title"]);

                foreach ($files as $file) {
                    $message->attach($file);
                }
            });
        }
        return ;
    }
}
