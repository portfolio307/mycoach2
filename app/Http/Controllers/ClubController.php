<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreClubRequest;
use App\Models\Club;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ClubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view("clubs.index", [
            'clubs' => Club::paginate(5)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view("clubs.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $club = new Club($request->all());
        $club['coach_id'] = Auth::id();
        $club->save();
        return redirect(route('clubs.your_club', Auth::id()));
    }

    /**
     * Display the specified resource.
     *
     * @param Club $club
     * @return View
     */
    public function show(Club $club): View
    {
        return view("clubs.show", [
            'club' => $club
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Club $club
     * @return View
     */
    public function edit(Club $club): View
    {
        return view("clubs.edit", [
            'club' => $club
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreClubRequest $request
     * @param Club $club
     * @return RedirectResponse
     */
    public function update(StoreClubRequest $request, Club $club): RedirectResponse
    {
        $club->fill($request->validated());
        $club->save();
        return redirect(route('clubs.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Club $club
     * @return RedirectResponse
     */
    public function destroy(Club $club): RedirectResponse
    {
        if ($club->delete()) {
            return redirect()->route('clubs.index');
        }
        return back();
    }

    public function your_club(User $user): View
    {
        if ($user['account_type'] == 1) {
            $club = Club::query()->where('coach_id', '=', $user['id'])->first();
        } else {
            if ($user['club_id'] != null)
                $club = Club::query()->where('id', '=', $user['club_id'])->first();
            else
                $club = [];
        }
        $all_clubs = Club::all();

        return view("clubs.your_club", [
            'club' => $club,
            'all_clubs' => $all_clubs,
            'user' => $user
        ]);
    }

    public function join_club(Request $request, User $user): RedirectResponse
    {
        $user = User::query()->where('id', '=', $user['id'])->first();
        $user['club_id'] = $request['club_id'];
        $user ->save();

        return redirect(route('clubs.your_club', Auth::id()));
    }

    public function leave_the_club(User $user): RedirectResponse
    {
        $user = User::query()->where('id', '=', $user['id'])->first();
        $user['club_id'] = null;
        $user ->save();

        return redirect(route('clubs.your_club', Auth::id()));
    }

}
