<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTournamentRequest;
use App\Models\Attempt;
use App\Models\Club;
use App\Models\Tournament;
use App\Models\User;
use Exception;
use http\Env\Response;
use http\QueryString;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class TournamentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view("tournaments.index", [
            'tournaments' => Tournament::paginate(5)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view("tournaments.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTournamentRequest $request
     * @return RedirectResponse
     */
    public function store(StoreTournamentRequest $request): RedirectResponse
    {
        $tournament = new Tournament($request->validated());
        $tournament['user_id'] = Auth::id();
        $tournament->save();
        $coach_club = Club::query()->where('coach_id', '=', Auth::id())->pluck('id')->first();

        $emails = DB::table('users')
            ->where('coach_id', '=', Auth::id())
            ->orWhere('club_id', '=', $coach_club)
            ->get('email');

        app('App\Http\Controllers\MailTournamentController')->index($emails, $tournament);

        return redirect(route('tournaments.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param Tournament $tournament
     * @return View
     */
    public function show(Tournament $tournament): View
    {
        $attempts = Attempt::all()->where('tournament_id', '=', $tournament['id']);
        $charges = User::all()->where('coach_id', '=', Auth::id());
        return view("tournaments.show", [
            'tournament' => $tournament,
            'attempts' => $attempts,
            'charges' => $charges
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Tournament $tournament
     * @return View
     */
    public function edit(Tournament $tournament): View
    {
        return view("tournaments.edit", [
            'tournament' => $tournament
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreTournamentRequest $request
     * @param Tournament $tournament
     * @return RedirectResponse
     */
    public function update(StoreTournamentRequest $request, Tournament $tournament): RedirectResponse
    {
        $tournament->fill($request->validated());
        $tournament->save();
        return redirect(route('tournaments.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Tournament $tournament
     * @return RedirectResponse
     */
    public function destroy(Tournament $tournament): RedirectResponse
    {
        if ($tournament->delete()) {
            return redirect()->route('tournaments.index');
        }
        return back();
    }

    public function show_statistics (Tournament $tournament): View
    {
        $attempts = Attempt::all()->where('tournament_id', '=', $tournament['id']);
        $sum_result = 0;
        $sum_rating = 0;
        $number_result = 0;
        $number_rating = 0;

        foreach ($attempts as $attempt)
        {
            $sum_result += $attempt['result'];
            $number_result += 1;
            $sum_rating += $attempt['rating'];
            $number_rating += 1;
        }
        $average_result = $sum_result / $number_result;
        $average_result = round($average_result, 2);
        $average_rating = $sum_rating / $number_rating;
        $average_rating = round($average_rating, 2);

        return view("tournaments.statistics", [
            'sum_result' => $sum_result,
            'number_result' => $number_result,
            'sum_rating' => $sum_rating,
            'number_rating' => $number_rating,
            'avg_result' => $average_result,
            'avg_rating' => $average_rating,
            'tournament' => $tournament
        ]);
    }
}
