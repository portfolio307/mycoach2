<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\Club;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return View
     */
    public function show(User $user): View
    {
        $coach_id = User::query()->where('id', '=', $user["coach_id"])->first();
        if ($coach_id == null) {
            $coach_id = "Brak trenera";
        } else {
            $coach_id = $coach_id['name'];
        }
        return view("users.show", [
            'user' => $user,
            'coach_id' => $coach_id
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return View
     */
    public function edit(User $user): View
    {
        $coach_id = User::query()->where('id', '=', $user["coach_id"])->first();
        $coaches = User::all()->where('account_type', '=', 1);
        $clubs = Club::all();
        if ($coach_id == null) {
            $coach_id = "Brak trenera";
        } else {
            $coach_id = $coach_id['name'];
        }

        return view("users.edit", [
            'user' => $user,
            'coach_id' => $coach_id,
            'coaches' => $coaches,
            'clubs' => $clubs
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserRequest $request
     * @param User $user
     * @return RedirectResponse
     */
    public function update(UserRequest $request, User $user): RedirectResponse
    {
        $user->fill($request->validated());
        if($user['photo'] != null)
        {
            $path = $request->file('photo')->store('photos');
            $user['photo'] = $path;
        }
        $user->save();
//        dd("test");
        return redirect(route('home'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    public function home_user()
    {
        $user = Auth::user();
        return (view("home", [
            'user' => $user
        ]));
    }
}
