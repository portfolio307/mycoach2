<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAttemptRequest;
use App\Models\Attempt;
use App\Models\Tournament;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AttemptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view("attempts.index", [
            'attempts' => Tournament::paginate(25)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Tournament  $tournament
     * @return View
     */
    public function create(Tournament $tournament): View
    {
        return view("attempts.create", [
            'tournament' => $tournament,
            'users' => User::all()->where('account_type', '=', 0 )
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreAttemptRequest  $request
     * @return RedirectResponse
     */
    public function store(StoreAttemptRequest $request): RedirectResponse
    {
        $attempt = new Attempt($request->all());
        $attempt -> save();
        return redirect(route('tournaments.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Attempt  $attempt
     * @return \Illuminate\Http\Response
     */
    public function show(Attempt $attempt)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Attempt  $attempt
     * @return View
     */
    public function edit(Attempt $attempt): View
    {
        $tournament = Tournament::query()->where('id', '=', $attempt['tournament_id'])->first();
        return view("attempts.edit", [
            'attempt' => $attempt,
            'tournament' => $tournament
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Attempt  $attempt
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attempt $attempt)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Attempt  $attempt
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attempt $attempt)
    {
        //
    }

    public function show_attempts_user(User $user)
    {
        $attempts = Attempt::all()->where('user_id', '=', $user['id']);

        return view("attempts.attempts_user",[
            'attempts' => $attempts,
            'user' => $user
        ]);
    }
}
