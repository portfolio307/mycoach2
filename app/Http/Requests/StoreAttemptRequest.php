<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAttemptRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            //StoreAttemptRequest
            'user_id' => 'required',
            'result' => 'numeric|min:0|max:100000|required',
            'rating' => 'integer|min:0|max:10|required',
            'tournament_id' => 'required'
        ];
    }
}
