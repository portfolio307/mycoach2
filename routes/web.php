<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('home'));
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/send-mail', [\App\Http\Controllers\MailTournamentController::class, 'index']);

Route::middleware(['auth'])->group(function() {

    Route::get('/tournaments', [App\Http\Controllers\TournamentController::class, 'index'])->name('tournaments.index');
    Route::get('/tournaments/create', [App\Http\Controllers\TournamentController::class, 'create'])->name('tournaments.create');
    Route::post('/tournaments', [App\Http\Controllers\TournamentController::class, 'store'])->name('tournaments.store');
    Route::get('/tournaments/{tournament}', [App\Http\Controllers\TournamentController::class, 'show'])->name('show');
    Route::get('/tournaments/edit/{tournament}', [App\Http\Controllers\TournamentController::class, 'edit'])->name('tournaments.edit');
    Route::post('/tournaments/{tournament}', [App\Http\Controllers\TournamentController::class, 'update'])->name('tournaments.update');
    Route::get('/tournaments/destroy/{tournament}', [App\Http\Controllers\TournamentController::class, 'destroy'])->name('tournaments.destroy');
    Route::get('/tournaments/show_statistics/{tournament}', [App\Http\Controllers\TournamentController::class, 'show_statistics'])->name('tournaments.show_statistics');

    Route::get('/clubs', [App\Http\Controllers\ClubController::class, 'index'])->name('clubs.index');
    Route::get('/clubs/create', [App\Http\Controllers\ClubController::class, 'create'])->name('clubs.create');
    Route::post('/clubs', [App\Http\Controllers\ClubController::class, 'store'])->name('clubs.store');
    Route::get('/clubs/{club}', [App\Http\Controllers\ClubController::class, 'show'])->name('clubs.show');
    Route::get('/clubs/edit/{club}', [App\Http\Controllers\ClubController::class, 'edit'])->name('clubs.edit');
    Route::post('/clubs/{club}', [App\Http\Controllers\ClubController::class, 'update'])->name('clubs.update');
    Route::get('/clubs/destroy/{club}', [App\Http\Controllers\ClubController::class, 'destroy'])->name('clubs.destroy');
    Route::get('/clubs/your_club/{user}', [App\Http\Controllers\ClubController::class, 'your_club'])->name('clubs.your_club');
    Route::post('/clubs/join_club/{user}', [App\Http\Controllers\ClubController::class, 'join_club'])->name('clubs.join_club');
    Route::get('/clubs/leave_the_club/{user}', [App\Http\Controllers\ClubController::class, 'leave_the_club'])->name('clubs.leave_the_club');

    Route::get('/users/{user}', [App\Http\Controllers\UserController::class, 'show'])->name('users.show');
    Route::get('/users/edit/{user}', [App\Http\Controllers\UserController::class, 'edit'])->name('users.edit');
    Route::post('/users/{user}', [App\Http\Controllers\UserController::class, 'update'])->name('users.update');

    Route::get('/attempts', [App\Http\Controllers\AttemptController::class, 'index'])->name('attempts.index');
    Route::get('/attempts/create/{tournament}', [App\Http\Controllers\AttemptController::class, 'create'])->name('attempts.create');
    Route::post('/attempts', [App\Http\Controllers\AttemptController::class, 'store'])->name('attempts.store');
    Route::get('/attempts/edit/{attempt}', [App\Http\Controllers\AttemptController::class, 'edit'])->name('attempts.edit');
    Route::post('/attempts/{attempt}', [App\Http\Controllers\AttemptController::class, 'update'])->name('attempts.update');
    Route::get('/attempts/show/{user}', [App\Http\Controllers\AttemptController::class, 'show_attempts_user'])->name('attempts.show_attempts_user');

});