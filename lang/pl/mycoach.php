<?php

return [
    'button' => [
        'create' => 'Utwórz',
        'save' => 'Zapisz',
        'add' => 'Dodaj',
        'add_attempt' => 'Dodaj próbe',
        'show_statistics' => 'Pokaż statystyki zawodów'
    ],
    'title' => [
        'actions' => 'Akcje',
        'edit_tournament' => 'Edycja turnieju',
        'tournament_preview' => 'Podląd turnieju',
        'list_of_tournaments' => 'Lista zawodów'

    ],
    'user' => [
        'show_title' => 'Podgląd twojego profilu',
        'edit_title' => 'Edycja twojego profilu',
        'your_profile' => 'Twój profil',
        'name' => 'Nazwa',
        'email' => 'Adres email',
        'account_type' => 'Typ konta',
        'coach_id' => 'Trener',
        'club_id' => 'Klub',
        'edit_profile' => 'Edytuj profil',
        'no_club' => 'Brak klubów',
        'photo' => 'Zdjęcie',
        'actual_photo' => 'Aktualne zdjęcie',
        'start' => 'Start',
        'no_unit' => 'Brak jednostki'
    ],
    'auth' => [
        'coach' => 'Trener',
        'photo' => 'Zdjęcie',
        'club_id' => 'Klub'
    ],
    'home' => [
        'welcome' => 'Witaj :Name',
        'list_of_tournaments' => 'Lista turniejów',
        'list_of_charges' => 'Lista podopiecznych'
    ],
    'tournament' => [
        'tournaments' => 'Turnieje',
        'tournament' => 'Turniej',
        'title' => 'Dodawanie zawodów',
        'name' => 'Nazwa',
        'description' => 'Opis',
        'date_and_time' => 'Data i czas',
        'attempts' => 'Podejścia',
        'charge' => 'Podopieczny',
        'statistics' => 'Styatystyki',
        'avg_result' => 'Średni wynik',
        'avg_rating' => 'Średnia ocena',
        'sum_result' => 'Suma wyników',
        'number_result' => 'Liczba wyników',
        'sum_rating' => 'Suma ocen',
        'number_rating' => 'Liczba ocen',
        'number_attempts' => 'Liczba prób',
        'unit' => 'Jednostka'
    ],
    'club' => [
        'clubs' => 'Kluby',
        'index_title' => 'Lista klubów',
        'create_title' => 'Tworzenie klubu',
        'preview_title' => 'Podgląd klubu',
        'edit_title' => 'Edycja klubu',
        'name' => 'Nazwa',
        'coach' => 'Trener',
        'description' => 'Opis',
        'no_club' => 'Brak klubu',
        'join_to_club' => 'Dołącz do klubu',
        'choose_a_club' => 'Wybierz klub',
        'create_a_club' => 'Utwórz klub',
        'leave_the_club' => 'Opuść klub',
        'delete_the_club' => 'Usuń klub'
    ],
    'attempt' => [
        'attempts' => 'Podejścia',
        'result' => 'Rezultat',
        'rating' => 'Ocena',
        'create_title' => 'Dodawanie podejścia',
        'result_for' => 'Rezultat dla',
        'list_of_attempts' => 'Lista podejść'
    ]
];